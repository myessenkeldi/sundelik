from sundelik.apps.core.models import User


class Configuration():
    def __init__(self, user):
        self.user = User.objects.filter(username=user).first()
        self.user_config = self.get_user_config()

    def get_user_config(self):
        return self.user.config.get('apps')

    def get_state(self, type):
        return self.user_config.get(type).get('status')

    def update_state(self, type):
        if self.get_state(type):
            self.user_config.get(type)['status'] = False
        else:
            self.user_config.get(type)['status'] = True
        self.user.save()




