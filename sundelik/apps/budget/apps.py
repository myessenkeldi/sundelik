from django.apps import AppConfig


class BudgetConfig(AppConfig):
    name = 'sundelik.apps.budget'
