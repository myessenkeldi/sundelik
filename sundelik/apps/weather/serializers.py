from rest_framework import serializers

from sundelik.apps.weather.models import WeatherCity


class WeatherCitySerializer(serializers.ModelSerializer):
    class Meta:
        model = WeatherCity
        fields = '__all__'