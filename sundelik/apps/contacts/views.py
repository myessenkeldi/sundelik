from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from sundelik.apps.contacts.models import Contact
from sundelik.apps.contacts.serializers import ContactSerializer


class ContactsList(generics.ListCreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = (IsAuthenticated,)


class ContactDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
