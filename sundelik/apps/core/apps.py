from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'sundelik.apps.core'

    def ready(self):
        import sundelik.apps.core.signals