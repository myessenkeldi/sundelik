from django.contrib.auth.models import AbstractUser, User
from django.db import models
from django.contrib.postgres.fields import JSONField


def default_config():
    return {"apps":{
            "task_manager":{"status":True},
            "be_social":{"status":True},
            "bd_notification":{"status":True}
        }
    }


class User(AbstractUser):

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    config = JSONField(null=True, blank=True, default=default_config, verbose_name=('Config'))


    def __str__(self):
        return self.username



class Notification(models.Model):
    pass
