from django.core.signals import request_finished
from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver


from sundelik.apps.contacts.models import Contact
from sundelik.apps.contacts.serializers import ElasticContactSerializer
from sundelik.es_client import es_client


@receiver(pre_save, sender=Contact, dispatch_uid="update_record")
def update_es_record(sender, instance, **kwargs):
    obj = ElasticContactSerializer(instance)
    obj.save(using=es_client)

@receiver(post_delete, sender=Contact, dispatch_uid="delete_record")
def delete_es_record(sender, instance, *args, **kwargs):
    obj = ElasticContactSerializer(instance)
    obj.delete(using=es_client, ignore=404)