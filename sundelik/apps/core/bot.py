import json
import os, sys
import logging

import requests
from django.db import transaction
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from telegram import ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, Filters, MessageHandler, ConversationHandler
import django
from django.contrib.auth.hashers import make_password

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sundelik.settings")
django.setup()

from sundelik.apps.core.utils import Configuration
from sundelik.settings import TELEGRAM_TOKEN
from sundelik.settings import api_url
from sundelik.apps.core.handler import first_task_handler_phase
from sundelik.apps.core.models import User
from sundelik.apps.core.strings import *

logger = logging.getLogger(__name__)

CONTACTS, TASKS, EXPENSES, WEATHER, CONFIGURATION = range(5)

SELECTING_ACTION, START_OVER = range(5,7)

FIND_CONTACT, WEATHER_CITY_DELETE, WEATHER_CITY_ADD = 7,8,9

WEATHER_ACTIONS = [WEATHER_CITY_DELETE, WEATHER_CITY_ADD]

MAIN_MENU = [CONTACTS, TASKS, EXPENSES, WEATHER, CONFIGURATION]


modules = {
    'Задача': first_task_handler_phase,
}


def menu(update, context):
    buttons = [[contacts_str], [tasks_str],[expenses_str],[weather_str],[configuration_str]]
    keyboard = ReplyKeyboardMarkup(buttons)
    text = main_menu_str
    # If we're starting over we don't need do send a new message
    if context.user_data.get(START_OVER):
        update.message.reply_text(text=text, reply_markup=keyboard)
    else:
        update.message.reply_text(first_greeting_str)
        update.message.reply_text(text=text, reply_markup=keyboard)

    context.user_data[START_OVER] = False
    return SELECTING_ACTION


def contacts_first(update, context):
    buttons = [[find_contact_str], [add_contact_str], [goback_str]]
    keyboard = ReplyKeyboardMarkup(buttons , one_time_keyboard=True)
    update.message.reply_text(text=choose_option_str, reply_markup=keyboard)
    return CONTACTS


def find_contact_first(update, context):
    update.message.reply_text(text='Введите критерий поиска в любом формате: ')
    return FIND_CONTACT


def find_contact_second(update, context):
    text = update.message.text
    update.message.reply_text(text='Пользователи по критерию '+text+': ')
    return CONTACTS


def add_contact(update, context):
    update.message.reply_text(text='Контакт добавлен')


def tasks_first(update, context):
    buttons = [[tasks_list_str], [new_task_str], [goback_str]]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=choose_option_str, reply_markup=keyboard)
    return TASKS


def task_list(update, context):
    update.message.reply_text(text='Список задач')


def new_task(update, context):
    update.message.reply_text(text='Новая задача добавлена!')


def expenses_first(update, context):
    buttons = [[add_expense_str], [expenses_today_str], [expenses_graph_str], [goback_str]]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=choose_option_str, reply_markup=keyboard)
    return EXPENSES


def add_expense(update, context):
    update.message.reply_text(text='Расход добавлен')


def expenses_today(update, context):
    update.message.reply_text(text='Список расходов на сегодня')


def expenses_graph(update, context):
    update.message.reply_text(text='График расходов')


def weather_first(update, context):
    buttons = [[weather_city_add_str], [weather_city_del_str], [goback_str]]
    resp = requests.get(url="{}{}".format(api_url,reverse('cities_list')))
    cities = json.loads(resp.text)
    for city in cities:
        buttons.insert(0,[city])
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=choose_option_str, reply_markup=keyboard)
    return WEATHER

def get_weather(update, context):
    city = update.message.text
    resp = requests.get(url = "{}{}".format(api_url, reverse('city', kwargs={'name': city})))
    update.message.reply_text(text=city_weather_str.format(frmt=json.loads(resp.text)))
    return WEATHER

def add_weather_city_first(update, context):
    buttons = [[goback_str]]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=write_new_city_str, reply_markup=keyboard)
    return WEATHER_CITY_ADD

def add_weather_city_second(update, context):
    resp = requests.post(url='{}{}'.format(api_url, reverse('city')), data={'name': update.message.text})
    if resp.status_code==201:
        update.message.reply_text(text = weather_city_add_success_str)
    return weather_first(update, context)

def del_weather_city_first(update, context):
    buttons = [[goback_str]]
    action = update.message.text
    context.user_data['weather_action'] = action
    resp = requests.get(url="{}{}".format(api_url,reverse('cities_list')))
    cities = json.loads(resp.text)
    for city in cities:
        buttons.insert(0,[city])
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=choose_city_str, reply_markup=keyboard)
    return WEATHER_CITY_DELETE


def del_weather_city_second(update, context):
    city = update.message.text
    resp = requests.delete(url = "{}{}".format(api_url, reverse('city', kwargs={'name': city})))
    update.message.reply_text(text='Город удален')
    return weather_first(update, context)

def configuration_first(update, context):
    user = update.message.from_user.username
    context.user_data[CONFIGURATION] = Configuration(user)
    first_button = '{} {}'.format(dr_ntf_str, str(context.user_data[CONFIGURATION].get_state('bd_notification')))
    second_button = '{} {}'.format(rel_ntf_str, str(context.user_data[CONFIGURATION].get_state('be_social')))
    buttons = [[first_button], [second_button], [goback_str]]
    keyboard = ReplyKeyboardMarkup(buttons)
    update.message.reply_text(text=choose_option_str, reply_markup=keyboard)
    return CONFIGURATION


def change_bd_ntf_state(update, context):
    context.user_data[CONFIGURATION].update_state('bd_notification')
    configuration_first(update, context)
    return CONFIGURATION


def change_rel_ntf_state(update, context):
    context.user_data[CONFIGURATION].update_state('be_social')
    configuration_first(update, context)
    return CONFIGURATION


def go_back(update, context):
    key, handler, check = conv_handler.check_update(update)
    state = conv_handler.conversations.get(key)
    context.user_data[START_OVER] = True
    if state in MAIN_MENU:
        menu(update,context)
        return SELECTING_ACTION
    elif state in WEATHER_ACTIONS:
        weather_first(update, context)
        return WEATHER


@csrf_exempt
def text_message(update, context):
    message_text = update.effective_message.text
    text_command = message_text.split()[0]
    if text_command in modules.keys():
        with transaction.atomic():
            username = update.effective_chat.username
            password = make_password(username)
            user = User.objects.get_or_create(username=username, defaults={'password': password})[0]
        response = modules[text_command](message_text, user)
        context.bot.send_message(chat_id=update.effective_chat.id, text=response)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text='Моя твоя не понимать')


if __name__ == '__main__':
    updater = Updater(TELEGRAM_TOKEN, use_context=True)
    dp = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points= [CommandHandler(('menu','start'), (menu))],
        states= {
            SELECTING_ACTION: [
                MessageHandler(Filters.regex('^({})$'.format(contacts_str)),
                               contacts_first),
                MessageHandler(Filters.regex('^({})$'.format(tasks_str)),
                               tasks_first),
                MessageHandler(Filters.regex('^({})$'.format(expenses_str)),
                               expenses_first),
                MessageHandler(Filters.regex('^({})$'.format(weather_str)),
                               weather_first),
                MessageHandler(Filters.regex('^({})$'.format(configuration_str)),
                               configuration_first),
            ],
            CONTACTS: [
                MessageHandler(Filters.regex('^({})$'.format(find_contact_str)),
                               find_contact_first),
                MessageHandler(Filters.regex('^({})$'.format(add_contact_str)),
                               add_contact),
                MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                               go_back)
            ],
            FIND_CONTACT:[
                MessageHandler(Filters.text, find_contact_second)
            ],
            TASKS: [
                MessageHandler(Filters.regex('^({})$'.format(new_task_str)),
                               new_task),
                MessageHandler(Filters.regex('^({})$'.format(tasks_list_str)),
                               task_list),
                MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                               go_back)
            ],
            EXPENSES: [
                MessageHandler(Filters.regex('^({})$'.format(add_expense_str)),
                               add_expense),
                MessageHandler(Filters.regex('^({})$'.format(expenses_today_str)),
                               expenses_today),
                MessageHandler(Filters.regex('^({})$'.format(expenses_graph_str)),
                               expenses_graph),
                MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                               go_back)
            ],
            WEATHER:[
                MessageHandler(Filters.regex('^({})$'.format(weather_city_add_str)),
                               add_weather_city_first),
                MessageHandler(Filters.regex('^({})$'.format(weather_city_del_str)),
                               del_weather_city_first),
                MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                               go_back),
                MessageHandler(Filters.regex('^([A-Z]{1}[a-z]*)$'),
                               get_weather)
            ],
            WEATHER_CITY_DELETE:[MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                                                go_back),
                                MessageHandler(Filters.regex('^([A-Z]{1}[a-z]*)|([a-z]*)$'),
                                                del_weather_city_second),
                                 ],
            WEATHER_CITY_ADD:[
                            MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                                             go_back),
                            MessageHandler(Filters.regex('^([A-Z]{1}[a-z]*)|([a-z]*)$'),
                                                add_weather_city_second),
                              ],
            CONFIGURATION: [
                MessageHandler(Filters.regex('^({}.*)$'.format(dr_ntf_str)),
                               change_bd_ntf_state),
                MessageHandler(Filters.regex('^({}.*)$'.format(rel_ntf_str)),
                               change_rel_ntf_state),
                MessageHandler(Filters.regex('^({})$'.format(goback_str)),
                               go_back)
            ],

        },
        fallbacks=[]
    )
    dp.add_handler(conv_handler)
    updater.dispatcher.add_handler(MessageHandler(Filters.text, text_message))
    updater.start_polling()
    updater.idle()
