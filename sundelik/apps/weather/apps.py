from django.apps import AppConfig


class WeatherConfig(AppConfig):
    name = 'sundelik.apps.weather'
