from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from sundelik.apps.weather.views import WeatherViewSet

cities_list = WeatherViewSet.as_view({
    'get': 'list',
})

city = WeatherViewSet.as_view({
    'post': 'add',
    'get':'retrieve',
    'delete':'delete'
})

urlpatterns = format_suffix_patterns([
    path('cities/', cities_list, name='cities_list'),
    path('city/', city, name='city'),
    path('city/<str:name>', city, name='city')
])