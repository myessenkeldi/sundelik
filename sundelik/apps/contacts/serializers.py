from rest_framework import serializers
from rest_framework_elasticsearch.es_serializer import ElasticModelSerializer

from sundelik.apps.contacts.models import Contact
from sundelik.apps.contacts.search_indexes import ContactIndex
from sundelik.apps.core.models import User


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'

class ElasticContactSerializer(ElasticModelSerializer):
    class Meta:
        model = Contact
        es_model = ContactIndex
        fields = ('name','surname', 'contact_description', 'birthday')
