from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from sundelik.apps.contacts import views

urlpatterns = [
    path('list/', views.ContactsList.as_view()),
    path('contact/update/<int:pk>', views.ContactDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)