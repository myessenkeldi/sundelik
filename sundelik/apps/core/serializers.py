from rest_framework import serializers


from sundelik.apps.core.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'config', 'email']


