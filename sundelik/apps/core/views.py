from django.http import HttpResponse
from rest_framework import generics


from sundelik.apps.core.models import User
from sundelik.apps.core.serializers import UserSerializer


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
