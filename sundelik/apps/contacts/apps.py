from django.apps import AppConfig


class ContactsConfig(AppConfig):
    name = 'sundelik.apps.contacts'

    def ready(self):
        import sundelik.apps.contacts.signals
