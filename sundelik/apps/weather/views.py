from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from sundelik.apps.weather.models import WeatherCity
from sundelik.apps.weather.serializers import WeatherCitySerializer

from sundelik.apps.weather import weather_api


class WeatherViewSet(viewsets.ModelViewSet):
    queryset = WeatherCity.objects.all()
    serializer_class = WeatherCitySerializer
    lookup_field = 'name'

    def list(self, request):
        cities = [city.name for city in WeatherCity.objects.all()]
        return Response(cities)

    def add(self, request):
        serializer = self.serializer_class(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):
        object = get_object_or_404(self.queryset, **kwargs)
        resp = weather_api.get_weather(object.name)
        return Response(resp)

    def delete(self, request, *args, **kwargs):
        object = get_object_or_404(self.queryset, **kwargs)
        object.delete()
        return Response('Deleted')