from django.contrib import admin

from sundelik.apps.tasks.models import Task

admin.site.register(Task)