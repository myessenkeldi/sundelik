from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'sundelik.apps.tasks'
