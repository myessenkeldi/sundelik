from django.db import models
from django.utils import timezone

from sundelik.apps.core.models import User


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, null=False )
    context = models.CharField(max_length=255, null=False, default='ничего не делать')
    date_created = models.DateTimeField(auto_now_add = True)
    execute_time = models.DateTimeField()
    done_status = models.BooleanField(default=False)

    def __str__(self):
        return self.name+' '+ self.context + ' Осталось времени: '+ str(self.execute_time-timezone.now())
