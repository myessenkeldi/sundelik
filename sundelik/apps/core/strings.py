# todo изменить здравствуйте на добрый день/вечер/утор в зависимости от времени
first_greeting_str = 'Здравствуйте!\n' \
                     'Вас привествует бот Sundelik,\n' \
                     'ваш личный помощник,\n' \
                     'Я помогу вам управлять вашими\n' \
                     'Контактами, Задачами и Расходами.'

current_configuration = 'Текущие настройки:\n' \
                        'Напоминание о днях рождения: {}\n' \
                        'Напоминание о связи: {}'

contacts_str = 'Контакты'
tasks_str = 'Задачи'
expenses_str = 'Расходы'
weather_str = 'Погода'
configuration_str = 'Настройки'

find_contact_str = 'Найти контакт'
add_contact_str = 'Добавить контакт'
goback_str = 'Назад'

tasks_list_str = 'Список задач'
today_str = 'Сегодня'
tomorrow_str = 'Завтра'
date_str = 'Дата'
calendar_str = 'Календарь'
new_task_str = 'Новая Задача'

add_expense_str = 'Добавить расход'
expeses_category_str = 'Категория'
expense_sum_str = 'Сумма'
expenses_today_str = 'Список расходов сегодня'
expenses_graph_str = 'График расходов'

weather_city_add_str = 'Добавить город'
weather_city_del_str = 'Удалить город'
city_weather_str = 'Погода в городе {frmt[name]} {frmt[temp]} градусов по °C'
choose_city_str = 'Выберите город: '
write_new_city_str = 'Напишите название города на латинице'
weather_city_add_success_str = 'Город успешно добавлен!'

dr_ntf_str = 'Напоминание о днях рождения: '
rel_ntf_str = 'Напоминание о связи: '

main_menu_str = 'Главное меню: '
choose_option_str = 'Выберите опцию:'



