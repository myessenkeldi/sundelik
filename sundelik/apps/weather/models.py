from django.db import models

# Create your models here.
class WeatherCity(models.Model):
    name = models.CharField(max_length=255, null=False, unique=True)

    def __str__(self):
        return self.name