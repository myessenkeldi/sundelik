from __future__ import (absolute_import, division, print_function, unicode_literals)

from elasticsearch_dsl import (
    DocType,
    Date,
    Keyword,
    Text,
    Boolean,
    Integer
)

class ContactIndex(DocType):
    name = Text(fields={'raw':Keyword()})
    surname = Text(fields={'raw':Keyword()})
    contact_description = Keyword(multi=True)
    birthday = Date()

    class Index:
        name = 'contact'
