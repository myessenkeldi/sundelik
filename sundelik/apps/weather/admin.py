from django.contrib import admin

# Register your models here.
from sundelik.apps.weather.models import WeatherCity

admin.site.register(WeatherCity)