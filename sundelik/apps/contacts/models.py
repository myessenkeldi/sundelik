from django.db import models
from sundelik.apps.core.models import User


class Contact(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    name = models.CharField(max_length=255, verbose_name= 'Имя')
    surname = models.CharField(max_length=255, verbose_name='Фаимилия')
    contact_description = models.CharField(max_length=255, verbose_name='Описание контакта')
    communication_level = models.IntegerField(default=5, verbose_name='Уровень коммуникации')
    birthday = models.DateField(verbose_name='День рождения')
    cpy = models.IntegerField(verbose_name='Комуницкаций ежегодно')

    def __str__(self):
        return '{surname} {name}'.format(name=self.name, surname=self.surname)

    def generate_birthday_notification(self):
        print ('birthday notification created')


# class ContactIndex(DocType):