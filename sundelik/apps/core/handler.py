from sundelik.apps.tasks.models import Task
from sundelik.apps.core.dateconfig import arb_days, month
from dateutil.parser import *
from datetime import *
import calendar

class TaskHandler:

    def __init__(self, task, user):
        self.user = user
        self.name = task['name']
        self.context = task['context']
        self.execute_time = task['execute_time']

    def save(self):
        task = Task(user= self.user, name=self.name, context= self.context, execute_time=self.execute_time)
        task.save()


def first_task_handler_phase(message, user):

    message = message.split()
    try:
        time = message.index('время')
    except:
        return 'Чувак ты забыл время!'
    try:
        task = {
            'name': message[0],
            'context': ' '.join(message[1:time]),
            'execute_time': format_datetime(message[time+1:])
        }
        print (task)
        task = TaskHandler(task, user)
        task.save()
        return 'Задача успешно сохранена!'
    except Exception as e:
        print(e)
        return 'Какая то дичь произошла, повтори еще раз'



def format_datetime(time):
    date, time = splitdates(time)
    expire_date_ = format(date=date, time=time)
    expire_date = parse(expire_date_)
    return expire_date

def splitdates(dttm):
    dvdr = dttm.index('в')
    return dttm[0:dvdr], dttm[dvdr+1:]

def format(date=None, time=None):
    if date:
        if date[0] in arb_days.keys():
            date = (datetime.now().date()+ timedelta(days=arb_days[date[0]]))
            date = (calendar.month_name[date.month][0:3]+' '+str(date.day))
        elif int(date[0]):
            day = date[0]
            month = get_month(date[1])
            date = month + ' '+ day
    if time:
        time = get_time(time)

    return date+' '+time

def get_month(substr):
    return (month[substr[0:3]][0:3])

def get_time(substr):
    return substr[0]