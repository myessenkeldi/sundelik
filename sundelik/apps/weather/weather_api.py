import requests

api_key = '71f7cfecdd48bb1158f1bfac609d6352'

def get_weather(city):
    url = "https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid={}".format(city, api_key)
    resp = requests.get(url).json()
    return handle_weather_resp(resp)


def handle_weather_resp(resp):
    weather_data = {}
    if resp.get('cod') and resp.get('cod')==200:
        weather_data.update(name=resp['name'],
                            temp=resp['main']['temp'],
                            code=resp['cod'])
    else:
        weather_data.update(code=resp['cod'])
    return weather_data

